
public class RBTree {

	private Node root = null;

	public void insert(RBTree T, Node<Integer> z) {

		assert (z != null);

		Node<Integer> y = null;
		Node<Integer> x = T.root;

		while (x != null) {
			y = x;
			if (z.key < x.key)
				x = x.left;
			else
				x = x.right;
		}

		z.p = y;
		if (y == null)
			T.root = z;
		else if (z.key < y.key)
			y.left = z;
		else
			y.right = z;
		z.left = null;
		z.right=null;
		z.color=true;
		insertFixup(T,z);
	}
	public void insertFixup(RBTree T,Node<Integer> z){
		
		while(z.p.color==true){
			if(z.p==z.p.p.left){
				Node<Integer> y=z.p.p.right;
				if(y.color==true){
					z.p.color=false;
					y.color=false;
					z.p.p.color=true;
					z=z.p.p;
				}
				else if(z==z.p.right){
					z=z.p;
					leftRotate(T,z);
				}
				z.p.color=false;
				z.p.p.color=true;
				rightRotate(T,z.p.p);
				
			}
			else{
				Node<Integer>y=z.p.p.left;
				if(y.color==true){
					z.p.color=false;
					y.color=false;
					z.p.p.color=true;
					z=z.p.p;
				}
				else if(z==z.p.left){
					z=z.p;
					rightRotate(T,z);
				}
				z.p.color=false;
				z.p.p.color=true;
				leftRotation(T,z.p.p);
				
			}
		}
		T.root.color=false;
	}
}
