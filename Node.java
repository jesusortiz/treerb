
public class Node<T> {

	public T key;
	public boolean color = false; // Red=true Black=false
	public Node right = null, left = null;
	public Node p = null;

	public Node(T k) {
		key = k;
	}

	public Node(T k, boolean c, Node l, Node r, Node p) {
		key = k;
		color = c;
		right = r;
		left = l;
		this.p = p;
	}

	public Node(T k, boolean c, Node l, Node r) {
		key = k;
		color = c;
		right = r;
		left = l;
		p = null;
	}

	public void setP(Node p) {
		this.p = p;
	}
	public T getKey() {
		return key;
	}

	public boolean checkRoot() {
		return (p == null && color == false);
	}

	public int checkRBProperty() {
		if (left != null && left.checkRBProperty() == -1)
			return -1;
		if (right != null && right.checkRBProperty() == -1)
			return -1;
		if (left == null) {
			if (right == null || right.checkRBProperty() == 1) {
				if (right != null && color == true && right.color == true)
					return -1;
				else
					return 1;
			}else
				return -1;
		}
		if (right == null) {
			if (left == null || left.checkRBProperty() == 1){
				if (left != null && color == true && left.color == true )
					return -1;
				else
					return 1;
			}else
				return -1;
		}

		if (color == true) {
			if (left == null && right.color == false)
				return 1;
			if (right == null && left.color == false)
				return 1;
			if (left.color == false && right.color == false)
				return 1;
			else
				return -1;
		}
          
		if (left.checkRBProperty() == right.checkRBProperty() && left.checkRBProperty() != -1)
			return 1;
		else
			return -1;
	}

}
