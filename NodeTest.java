import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class NodeTest {

	private Node r = new Node(10, true, null, null, null);
	private Node n = new Node(19, false, null, null, null);

	@Test
	public void testCheckRoot() {
		Node root = new Node(2, true, r, n, null);
		assertEquals(false, root.checkRoot());
		Node root2 = new Node(5, false, r, n, null);
		assertEquals(true, root2.checkRoot());
	}

	@Test
	public void testCheckRBProperty1() {
		// Creation of RedBlack Tree
		Node b = new Node(10, true, r, null);
		Node c = new Node(11, true, null, r);
		Node d = new Node(11, true, null, null,c);
		Node a = new Node(12, false, b, c, null);
		b.setP(a);
		c.setP(a);
		r.setP(b);
		
		assertEquals(-1, a.checkRBProperty());
		assertEquals(-1, b.checkRBProperty());
		assertEquals(-1, c.checkRBProperty());
		assertEquals(1, r.checkRBProperty());
		assertEquals(1, d.checkRBProperty());
	}
	@Test
	public void testCheckRBProperty2() {
		// Creation of RedBlack Tree
		Node b = new Node(10, true, n,r);
		Node a = new Node(12, false, b, null, null);
		n.setP(b);
		r.setP(b);
		b.setP(a);
		
		assertEquals(-1, a.checkRBProperty());
		assertEquals(-1, b.checkRBProperty());
		assertEquals(1, n.checkRBProperty());
		assertEquals(1, r.checkRBProperty());
	}
	
	@Test
	public void testCheckRBProperty3() {
		// Creation of RedBlack Tree
		Node b = new Node(10, true, n,r);
     	Node a = new Node(12, false, null, b, null);
		b.setP(a);
		n.setP(b);
		r.setP(b);
		
		assertEquals(-1, a.checkRBProperty());
		assertEquals(-1, b.checkRBProperty());
		assertEquals(1, r.checkRBProperty());
		assertEquals(1, n.checkRBProperty());
	}
	@Test
	public void testCheckRBProperty4() {
		// Creation of RedBlack Tree
		Node e = new Node(10, true, null, null, null);
		Node d = new Node(11, false, e, r);
		Node b = new Node(12, true, n, d);
		Node c = new Node(11, false,null,null);
		Node a = new Node(31, false,b,c);
		e.setP(d);
		r.setP(d);
		d.setP(b);
		n.setP(b);
		b.setP(a);
		c.setP(a);

		assertEquals(1, a.checkRBProperty());
		assertEquals(1, b.checkRBProperty());
		assertEquals(1, c.checkRBProperty());
		assertEquals(1, d.checkRBProperty());
		assertEquals(1, e.checkRBProperty());
		assertEquals(1, r.checkRBProperty());
		assertEquals(1, n.checkRBProperty());
	}

}
